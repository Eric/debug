<?php

/**
 * Fonctions spécifiques à une valeur
 *
 * @package SPIP\valeurs\selection
**/


// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérifie que la valeur postée
 * correspond aux valeurs proposées lors de la config de valeur
 * @param string $valeur la valeur postée
 * @param array $description la description de la saisie
 * @return bool true si valeur ok, false sinon,
**/
function choix_grille_bem_valeurs_acceptables($valeur, $description) {

	$acceptable = false;
	$options = $description['options'];
	if ($valeur == '' and !isset($options['obligatoire'])) {
		$acceptable = true;
	} elseif (saisies_verifier_gel_saisie($description) and isset($options['defaut'])) {
		$acceptable = ($valeur == $options['defaut']);
	} else {
		$data = array();
		$name = $options['block'];
		$cols = saisies_chaine2tableau($options['data_cols']);
		$rows = saisies_chaine2tableau($options['data_rows']);
		foreach ($rows as $row => $label_row) {
			foreach ($cols as $col => $label_col) {
				if ($col) {
					$value = $name . ($row ? '_' . $row : '') . '_' . $col;
					$data[] = $value;
				}
			}
		}
		$acceptable = in_array($valeur, $data);
	}

	return $acceptable;
}

