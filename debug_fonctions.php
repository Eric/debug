<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Une fonction récursive pour joliment afficher un tableau ou une chaine issue
 * de la sérialisation d'un tableau.
 *
 * @param array|string $table             Tableau à afficher ou chaine issue de la sérialisation d'un tableau
 * @param string       $afficher_en_clair si `oui` indique qu'il faut afficher la chaine vide, la valeur null
 *                                        et les booleens respectivement comme `''`, `null`, `true` ou `false`.
 *
 * @return string Chaîne html affichant une balise `<table>`
**/
if (!function_exists('table_afficher')) {
	function table_afficher($table, $afficher_en_clair = 'oui') {
		// Retour en erreur : chaine vide
		$html = '';

		// Eviter que l'argument $table soit null
		if (null === $table) {
			$table = [];
		}

		// Si la variable à afficher est une chaine, on vérifie que c'est la chaine issue d'une sérialisation de tableau.
		if (
			is_string($table)
			and is_array($variable_deserialisee = unserialize($table))
		) {
			$table = $variable_deserialisee;
		}

		if (
			$table
			and is_array($table)
		) {
			$style = " style='border:1px solid #ddd;'";
			$html = "<table style='border-collapse:collapse;'>\n";
			foreach ($table as $_champ => $_valeur) {
				$valeur_affichee = '';
				if (
					is_array($_valeur)
					|| is_array(@unserialize($_valeur))
				) {
					if ($_valeur) {
						// Récursion sur le sous-tableau
						$valeur_affichee = table_afficher($_valeur, $afficher_en_clair);
					} elseif ($afficher_en_clair === 'oui') {
						$valeur_affichee = '<i>[]</i>';
					}
				} elseif ($afficher_en_clair === 'oui') {
					if ($_valeur === null) {
						$valeur_affichee = '<i>null</i>';
					} elseif ($_valeur === 0) {
						$valeur_affichee = "<i>0</i>";
					} elseif ($_valeur === '') {
						$valeur_affichee = "<i>''</i>";
					} elseif ($_valeur === true) {
						$valeur_affichee = '<i>true</i>';
					} elseif ($_valeur === false) {
						$valeur_affichee = '<i>false</i>';
					} elseif ($_valeur === "\t") {
						$valeur_affichee = '<i>\t</i>';
					} elseif ($_valeur === "\n") {
						$valeur_affichee = '<i>\n</i>';
					} elseif ($_valeur === "\r") {
						$valeur_affichee = '<i>\r</i>';
					} elseif ($_valeur === "\r\n") {
						$valeur_affichee = '<i>\r\n</i>';
					} elseif (!trim($_valeur)) {
						$valeur_affichee = "<i>'{$_valeur}'</i>";
					} else {
						$valeur_affichee = entites_html($_valeur);
					}
				} elseif ($_valeur) {
					// Si on ne veut pas afficher les valeurs spéciales en clair on remplace les caractères spéciaux
					$valeur_affichee = entites_html(str_replace(['&quot;', '&#039;'], ['"', '\''], $_valeur));
				}

				$html .= "<tr>\n<td{$style}><strong>"
					. entites_html($_champ)
					. "&nbsp;:&nbsp;</strong></td><td{$style}>"
					. $valeur_affichee
					. "</td>\n</tr>\n";
			}

			// On ferme la table en cours
			$html .= '</table>';
		}

		return $html;
	}
}

function balise_ENVDUMP_dist($p) {
	if (!function_exists('dump')) {
		throw new \Error('dump fonction is needed to use #DUMP');
	}
	$p->code = 'vide(dump($Pile[0]))';

	return $p;
}

include_spip('debug_tests');

include_spip('inc/ezmashup_feed');
