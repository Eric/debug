<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
if (!defined('_EXTRAIRE_IDIOME')) {
	/**
	 * Extrait les composants d'un idiome de langue utilisable dans un YAML
	 * '@<:(?:([a-z0-9_]+):)?([a-z0-9_]+):>@isS'.
	 */
	define('_EXTRAIRE_IDIOME', ',<:(?:([a-z0-9_]+):)?([a-z0-9_]*):/?>,iS');
}


function debug_geometrie($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/ezmath_geometrie');
		$point1 = [40.7127, -74.0059];
		$point2 = [34.0500, -118.2500];
		$point1 = [51, 2];
		$point2 = [51, 3];
		$distance = sphere_distance_euclidienne($point1, $point2);
		$retour = var_export($distance, true);
		$html .= "<pre>p1(lat,lon)<strong>({$point1[0]},{$point1[1]})</strong> - p2(lat,lon)<strong>({$point2[0]},{$point2[1]})</strong>
<br />distance euclidienne = {$retour}</pre>";

		$distance = sphere_distance_loi_sinus($point1, $point2);
		$retour = var_export($distance, true);
		$html .= "<pre>p1(lat,lon)<strong>({$point1[0]},{$point1[1]})</strong> - p2(lat,lon)<strong>({$point2[0]},{$point2[1]})</strong>
<br />distance loi sinus = {$retour}</pre>";

		$distance = sphere_distance_haversine($point1, $point2);
		$retour = var_export($distance, true);
		$html .= "<pre>p1(lat,lon)<strong>({$point1[0]},{$point1[1]})</strong> - p2(lat,lon)<strong>({$point2[0]},{$point2[1]})</strong>
<br />distance haversine = {$retour}</pre>";

	}

	return $html;
}

function debug_objet_info($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('base/objets');
		include_spip('inc/filtres');

		$objet = 'spip_articles';
		$info = 'type';
		$res = objet_info($objet, $info);
		$retour = var_export($res, true);
		$html .= "<pre><strong>Objet : {$objet}, info : $info</strong> <br />{$retour}</pre>";

		$objet = 'articles';
		$info = 'table_objet';
		$res = objet_info($objet, $info);
		$retour = var_export($res, true);
		$html .= "<pre><strong>Objet : {$objet}, info : $info</strong> <br />{$retour}</pre>";

		$objet = 'article';
		$info = 'page';
		$res = objet_info($objet, $info);
		$retour = var_export($res, true);
		$html .= "<pre><strong>Objet : {$objet}, info : $info</strong> <br />{$retour}</pre>";

		$objet = 'article';
		$info = 'info_1_objet';
		$res = objet_info($objet, $info);
		$retour = var_export($res, true);
		$html .= "<pre><strong>Objet : {$objet}, info : $info</strong> <br />{$retour}</pre>";

		$trouver_table = charger_fonction('trouver_table', 'base');
		$table = 'spip_articles';
		$desc = $trouver_table($table);
		$retour = var_export($desc, true);
		$html .= "<pre><strong>Table : {$table}</strong> <br />{$retour}</pre>";

		$trouver_table = charger_fonction('trouver_table', 'base');
		$table = 'spip_auteurs';
		$desc = $trouver_table($table);
		$retour = var_export($desc, true);
		$html .= "<pre><strong>Table : {$table}</strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_get_infos_plugin($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('plugins/get_infos');

		$cache = '';
		$prefixe = 'eric/taxonomie';
		$cache = plugins_get_infos_dist($prefixe, true);
		$retour = var_export($cache, true);
		$html .= "<pre><strong>Paquet du plugin {$prefixe}</strong> <br />{$retour}</pre>";
	}

	return $html;
}
function debug_taxon_merger_traductions($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/taxonomie');
		$jeu = [
			['', '', ''],
			['', '', '<multi></multi>'],
			['', '', '<multi> </multi>'],
			['<multi>[fr]en français</multi>','', '<multi>[fr]en français</multi>'],
			['<multi>[fr]en français</multi>', '', '[fr]en français'],
			['', '<multi></multi>', ''], ['', '<multi> </multi>', ''],
			['<multi>[fr]en français</multi>', '<multi>[fr]en français</multi>', ''],
			['<multi>[fr]en français</multi>', '[fr]en français', ''],
			['', '<multi></multi>', '<multi></multi>'],
			['<multi>[en]in English[fr]en français</multi>', '<multi>[fr]en français</multi>', '<multi>[en]in English</multi>'],
			['<multi>[en]in English[fr]en français 1</multi>', '<multi>[fr]en français 1</multi>', '<multi>[fr]en français 2[en]in English</multi>'],
			['<multi>par defaut[fr]en français</multi>', '<multi>[fr]en français</multi>', '<multi>par defaut</multi>'],
			['<multi>par defaut[fr]en français</multi>', '<multi>par defaut </multi>', '<multi>[fr]en français </multi>'],
		];

		foreach ($jeu as $_numero => $_multi) {
			$result = $_multi[0];
			$prio = $_multi[1];
			$non_prio = $_multi[2];
			$merge = taxon_merger_traductions($prio, $non_prio);
			$couleur = ($merge == $result) ? 'green' : 'red';
			$html .= "<dt>Cas {$_numero} : prio='<code class='spip_code'>{$prio}</code>' - non_prio='<code class='spip_code'>{$non_prio}</code>' - resultat attendu='<code class='spip_code'>{$result}</code>'</dt>";
			$html .= "<dd style='color:{$couleur}'>=> '<code class='spip_code'>{$merge}</code>'</code></dd>";
		}
		if ($html) {
			$html = "<dl>{$html}</dl>";
		}
	}

	return $html;
}

function debug_ezcodec($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/ezcodec');
		include_spip('inc/flock');
		$options = [];

		// Format XML
		$contenu = <<<EOD
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ISO_4217 Pblshd="2023-01-01">
	<CcyTbl>
		<CcyNtry>
			<CtryNm>AFGHANISTAN</CtryNm>
			<CcyNm>Afghani</CcyNm>
			<Ccy>AFN</Ccy>
			<CcyNbr>971</CcyNbr>
			<CcyMnrUnts>2</CcyMnrUnts>
		</CcyNtry>
		<CcyNtry>
			<CtryNm>ÅLAND ISLANDS</CtryNm>
			<CcyNm>Euro</CcyNm>
			<Ccy>EUR</Ccy>
			<CcyNbr>978</CcyNbr>
			<CcyMnrUnts>2</CcyMnrUnts>
		</CcyNtry>
	</CcyTbl>
</ISO_4217>
EOD;

		$options['with_root'] = false;
		$tab = contenu_decoder($contenu, 'xml', $options);
		$retour = var_export($tab, true);
		$html .= "<pre><strong>tableau issu XML</strong> <br />{$retour}</pre>";

		$options['with_root'] = true;
		$tab = contenu_decoder($contenu, 'xml', $options);
		$retour = var_export($tab, true);
		$html .= "<pre><strong>tableau issu XML</strong> <br />{$retour}</pre>";

		$contenu = <<<EOD
<root>
 <title>Forty What?</title>
 <from>Joe</from>
 <to><cc>Jane</cc></to>
 <contenu>
  I know that's the answer -- but what's the question?
 </contenu>
</root>
EOD;

		$options['with_root'] = true;
		$tab = contenu_decoder($contenu, 'xml', $options);
		$retour = var_export($tab, true);
		$html .= "<pre><strong>tableau issu XML</strong> <br />{$retour}</pre>";

		$texte = '';
		$retour = contenu_encoder($tab, 'xml', $options);
		$fichier = _DIR_TMP . 'ezcodec/xml_1_' . date('Ymd-His');
		ecrire_fichier($fichier, $retour);
		$html .= "<pre><strong>chaine XML dans le fichier : {$fichier}</strong></pre>";

		$contenu = <<<EOD
<root>
 <tag1 class="tata">Forty What?</tag1>
 <tag2>Joe</tag2>
 <tag3><cc>Jane</cc></tag3>
 <tag4 alias="toto" class="tata">
  I know that's the answer -- but what's the question?
 </tag4>
</root>
EOD;

		$options['with_root'] = true;
		$tab = contenu_decoder($contenu, 'xml', $options);
		$retour = var_export($tab, true);
		$html .= "<pre><strong>tableau issu XML</strong> <br />{$retour}</pre>";

		$texte = '';
		$retour = contenu_encoder($tab, 'xml', $options);
		$fichier = _DIR_TMP . 'ezcodec/xml_2_' . date('Ymd-His');
		ecrire_fichier($fichier, $retour);
		$html .= "<pre><strong>chaine XML dans le fichier : {$fichier}</strong></pre>";

		$contenu = <<<EOD
<boussole alias="spip" demo="https://boussole.spip.net/">

	<groupe type="reference">
		<site alias="net" src="https://www.spip.net/" actif="oui" />
		<site alias="doc" src="https://code.spip.net/" actif="oui" />
		<site alias="programmer" src="https://programmer.spip.net/" actif="oui" />
	</groupe>

	<groupe type="extension">
		<site alias="plugin" src="https://plugins.spip.net/" actif="oui" />
		<site alias="plugincode" src="https://code.plugins.spip.net/" actif="non" />
		<site alias="contrib" src="https://contrib.spip.net/" actif="oui" />
		<site alias="trad" src="https://trad.spip.net/" actif="oui" />
		<site alias="core" src="https://core.spip.net/" actif="non" />
		<site alias="zone" src="https://zone.spip.org/" actif="non" />
		<site alias="forge" src="https://git.spip.net/" actif="oui" />
	</groupe>

	<groupe type="aide">
		<site alias="forum" src="https://forum.spip.net/" actif="non" />
		<site alias="discuter" src="https://discuter.spip.net/" actif="oui" />
		<site alias="user" src="http://listes.rezo.net/mailman/listinfo/spip/" actif="non" />
		<site alias="irc" src="https://irc.spip.net/" actif="oui" />
		<site alias="party" src="https://party.spip.net/" actif="oui" />
		<site alias="edgard" src="https://edgard.spip.net/" actif="oui" />
	</groupe>

	<groupe type="decouverte">
		<site alias="info" src="http://info.spip.net/" actif="non" />
		<site alias="herbier" src="http://herbier.spip.net/" actif="non" />
		<site alias="video" src="https://medias.spip.net/" actif="oui" />
		<site alias="demo" src="https://demo.spip.net/" actif="oui" />
		<site alias="test" src="http://grml.eu/" actif="oui" />
		<site alias="syntaxe" src="https://syntaxe.spip.net/" actif="oui" />
	</groupe>

	<groupe type="actualite">
		<site alias="blog" src="https://blog.spip.net/" actif="oui" />
		<site alias="twit" src="http://spip.org/" actif="non" />
		<site alias="sedna" src="https://sedna.spip.net/" actif="oui" />
		<site alias="zine" src="http://zine.spip.net/" actif="non" />
		<site alias="mag" src="http://mag.spip.net/" actif="non" />
	</groupe>

</boussole>
EOD;

		$tab = contenu_decoder($contenu, 'xml', $options);
		$retour = var_export($tab, true);
		$html .= "<pre><strong>tableau issu XML</strong> <br />{$retour}</pre>";

		$texte = '';
		$retour = contenu_encoder($tab, 'xml', $options);
		$fichier = _DIR_TMP . 'ezcodec/xml_3_' . date('Ymd-His');
		ecrire_fichier($fichier, $retour);
		$html .= "<pre><strong>chaine XML dans le fichier : {$fichier}</strong></pre>";

		// Format YAML
		$contenu = <<<EOD
title: 'Population des comtés US en 2020'
description: ''
is_editable: true
category: territory_data
tags:
  _type_id: iso_territoire
  type: infrasubdivision
  pays: US
target:
  format: sql_table
  id: territoires_extras
  options:
    stop_on_error: false
mapping:
  basic_fields:
    iso_territoire: Fips
    valeur: Population
  static_fields:
    extra: population
    type_extra: stat
    feed_id: /feed_id
    type: '#type'
    iso_pays: '#pays'
sources_basic:
  basic_1:
    source: { type: file, format: csv, uri: source_basic_1.csv, last_update: '', version: '', license: '' }
    decoding: { delimiter: ; }
    provider: { name: '', url: '' }
EOD;

		$tab = contenu_decoder($contenu, 'yaml', $options);
		$retour = var_export($tab, true);
		$html .= "<pre><strong>tableau issu YAML</strong> <br />{$retour}</pre>";

		$str = contenu_encoder($tab, 'yaml', $options);
		$retour = var_export($str, true);
		$html .= "<pre><strong>chaine YAML</strong> <br />{$retour}</pre>";

		// Format JSON
		$contenu = '{"donnees":{"ville":"Paris","pays":"France","pays_iso2":"FR","region":"Europe","longitude":2.351,"latitude":48.857},"extras":{"credits":{"titre":"AccuWeather","logo":"services\/images\/accuweather.png","lien":"https:\/\/www.accuweather.com\/"},"config":{"unite":"m","condition":"weather","theme_local":"original","theme_weather":"sticker","theme":"","source_api":["ville","pays","pays_iso2","region","longitude","latitude"],"source_calcul":[],"source_none":[],"nom_service":"AccuWeather"},"lieu":"623","mode":"infos","periodicite_cache":0,"service":"accuweather","erreur":{"type":"","service":{"code":"","message":""}}}}';

		$tab = contenu_decoder($contenu, 'json', $options);
		$retour = var_export($tab, true);
		$html .= "<pre><strong>tableau issu JSON</strong> <br />{$retour}</pre>";

		$str = contenu_encoder($tab, 'json', $options);
		$retour = var_export($str, true);
		$html .= "<pre><strong>chaine JSON</strong> <br />{$retour}</pre>";

		// Format CSV
		$contenu = 'iso;type;code
BE-BRU;code_ins_reg;04000
BE-VAN;code_ins;10000
BE-VBR;code_ins;20001
BE-VLG;code_ins_reg;02000
BE-VLI;code_ins;70000
BE-VOV;code_ins;40000
BE-VWV;code_ins;30000
BE-WAL;code_ins_reg;03000
BE-WBR;code_ins;20002
BE-WHT;code_ins;50000
BE-WLG;code_ins;60000
BE-WLX;code_ins;80000
BE-WNA;code_ins;90000';

		$options['delim'] = ';';
		$tab = contenu_decoder($contenu, 'csv', $options);
		$retour = var_export($tab, true);
		$html .= "<pre><strong>tableau issu CSV</strong> <br />{$retour}</pre>";

		$options['delim'] = ',';
		$str = contenu_encoder($tab, 'csv', $options);
		$retour = var_export($str, true);
		$html .= "<pre><strong>chaine CSV avec virgule</strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_rainette_date_heure($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/rainette_normaliser');

		$html .= '<h3>donnee_formater_heure()</h3>';

		$valeur = 12;
		$heure = donnee_formater_heure($valeur);
		$retour = var_export($heure, true);
		$html .= "<pre><strong>{$valeur}</strong> <br />{$retour}</pre>";

		$valeur = 1234;
		$heure = donnee_formater_heure($valeur);
		$retour = var_export($heure, true);
		$html .= "<pre><strong>{$valeur}</strong> <br />{$retour}</pre>";

		$valeur = 1700403300;
		$date = date('Y-m-d H:i:s', $valeur);
		$heure = donnee_formater_heure($valeur);
		$retour = var_export($heure, true);
		$html .= "<pre><strong>{$valeur} - {$date}</strong> <br />{$retour}</pre>";

		$valeur = '2023-11-19T07:00:00+01:00';
		$heure = donnee_formater_heure($valeur);
		$retour = var_export($heure, true);
		$html .= "<pre><strong>{$valeur}</strong> <br />{$retour}</pre>";

		$valeur = '2023-11-19 18:00:00';
		$heure = donnee_formater_heure($valeur);
		$retour = var_export($heure, true);
		$html .= "<pre><strong>{$valeur}</strong> <br />{$retour}</pre>";

		$html .= '<h3>donnee_formater_date()</h3>';

		$valeur = 1700403300;
		$date = donnee_formater_date($valeur);
		$retour = var_export($date, true);
		$html .= "<pre><strong>{$valeur}</strong> <br />{$retour}</pre>";

		$valeur = '2023-11-19T07:00:00+01:00';
		$date = donnee_formater_date($valeur);
		$retour = var_export($date, true);
		$html .= "<pre><strong>{$valeur}</strong> <br />{$retour}</pre>";

		$valeur = '2023-11-19 18:00:00';
		$date = donnee_formater_date($valeur);
		$retour = var_export($date, true);
		$html .= "<pre><strong>{$valeur}</strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_sarka($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('public/sarkaspip_balises');
		$rubriques = calcul_rubrique_specialisee('agenda', 'secteur', 'in');
		$retour = var_export($rubriques, true);
		$html .= "<pre><strong>AGENDA SECTEUR IN</strong> <br />{$retour}</pre>";
	}

	return $html;
}
function debug_couleur($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/filtres_images_mini');
		include_spip('inc/filtres_images_lib_mini');
		include_spip('filtres/couleurs');
		include_spip('inc/cartes_choroplethes_couleur');

		$html .= '<h3>TRANSFORMATIONS</h3>';

		$html .= '<h3>MELANGES</h3>';

		$couleur1 = 'red';
		$couleur2 = 'blue';
		$couleur = couleur_melanger($couleur1, $couleur2);
		$retour = var_export($couleur, true);
		$html .= "<pre><strong>NO COEFF - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		$couleur_fg = '#' . couleur_inverser($couleur);
		$html .= "<li style=\"background-color:{$couleur}; color:{$couleur_fg}\">{$couleur}</li>";

		$couleur1 = 'red';
		$couleur2 = 'blue';
		$options = ['methode' => 'equal_coefficient'];
		$couleur = couleur_melanger($couleur1, $couleur2, $options);
		$retour = var_export($couleur, true);
		$html .= "<pre><strong>EQUAL COEFF - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		$couleur_fg = '#' . couleur_inverser($couleur);
		$html .= "<li style=\"background-color:{$couleur}; color:{$couleur_fg}\">{$couleur}</li>";

		$couleur1 = 'red';
		$couleur2 = 'blue';
		$options = ['methode' => 'unique_coefficient', 'coefficient' => 0.5];
		$couleur = couleur_melanger($couleur1, $couleur2, $options);
		$retour = var_export($couleur, true);
		$html .= "<pre><strong>ONE COEFF {$options['coefficient']} - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		$couleur_fg = '#' . couleur_inverser($couleur);
		$html .= "<li style=\"background-color:{$couleur}; color:{$couleur_fg}\">{$couleur}</li>";

		$couleur1 = 'red';
		$couleur2 = 'blue';
		$options = ['methode' => 'unique_coefficient', 'coefficient' => 0.2];
		$couleur = couleur_melanger($couleur1, $couleur2, $options);
		$retour = var_export($couleur, true);
		$html .= "<pre><strong>ONE COEFF {$options['coefficient']} - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		$couleur_fg = '#' . couleur_inverser($couleur);
		$html .= "<li style=\"background-color:{$couleur}; color:{$couleur_fg}\">{$couleur}</li>";

		$couleur1 = 'red';
		$couleur2 = 'blue';
		$options = ['methode' => 'unique_coefficient', 'coefficient' => 0.8];
		$couleur = couleur_melanger($couleur1, $couleur2, $options);
		$retour = var_export($couleur, true);
		$html .= "<pre><strong>ONE COEFF {$options['coefficient']} - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		$couleur_fg = '#' . couleur_inverser($couleur);
		$html .= "<li style=\"background-color:{$couleur}; color:{$couleur_fg}\">{$couleur}</li>";

		$couleur1 = 'red';
		$couleur2 = 'blue';
		$options = ['methode' => 'rgb_coefficient', 'coefficient' => ['red' => 0.8, 'green' => 0.5, 'blue' => 0.2]];
		$couleur = couleur_melanger($couleur1, $couleur2, $options);
		$retour = var_export($couleur, true);
		$html .= '<pre><strong>RGB COEFF ' . implode(',', $options['coefficient']) . " - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		$couleur_fg = '#' . couleur_inverser($couleur);
		$html .= "<li style=\"background-color:{$couleur}; color:{$couleur_fg}\">{$couleur}</li>";
	}

	return $html;
}

function debug_matrice($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/cartes_choroplethes_matrice');

		$matrix1 = [
			[1, 2],
			[3, 4],
		];
		$matrix2 = [
			[5, 6],
			[7, 8],
		];

		$matrix = matrice_multiplier($matrix1, $matrix2);
		$retour = var_export($matrix, true);
		$html .= "<pre><strong></strong> <br />{$retour}</pre>";

		$matrix = matrice_transposer($matrix1);
		$retour = var_export($matrix, true);
		$html .= "<pre><strong></strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_palette($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/filtres_images_mini');
		include_spip('inc/filtres_images_lib_mini');
		include_spip('filtres/couleurs');
		include_spip('inc/ezmath_palette');

		var_dump(_couleur_hex_to_dec(''));

		$html .= '<h3>PALETTES</h3>';

		$couleur = '#c40a0a';
		$palette = palette_teinte_unique($couleur, 7);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>SINGLE HUE - Couleur {$couleur}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur = '#000';
		$palette = palette_teinte_unique($couleur, 7);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>SINGLE HUE - Couleur {$couleur}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur = '#F44336';
		$palette = palette_teinte_unique($couleur, 4, 0.1, 0.9);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>SINGLE HUE - Couleur {$couleur}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur1 = '#0000ff';
		$couleur2 = '#ff0000';
		$palette = palette_teinte_bipolaire($couleur1, $couleur2, 7);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>BI-POLAR HUE - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur1 = '#ffff81';
		$couleur2 = '#6b0100';
		$palette = palette_couleur_melangee($couleur1, $couleur2, 7);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>COLOR BLEND - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur1 = '#4CAF50';
		$couleur2 = '#FFEB3B';
		$palette = palette_couleur_melangee($couleur1, $couleur2, 4);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>COLOR BLEND - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur1 = 'brown';
		$couleur2 = 'blue';
		$palette = palette_couleur_melangee($couleur1, $couleur2, 7);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>COLOR BLEND - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur1 = 'yellow';
		$couleur2 = 'red';
		$palette = palette_couleur_melangee($couleur1, $couleur2, 7);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>COLOR HEATMAP - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur1 = 'yellow';
		$couleur2 = 0.8;
		$palette = palette_teinte_melangee($couleur1, $couleur2, 7);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>HUE BLEND - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur1 = '#4CAF50';
		$couleur2 = 0.6;
		$palette = palette_teinte_melangee($couleur1, $couleur2, 4);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>HUE BLEND - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}

		$couleur1 = 'brown';
		$couleur2 = 0.8;
		$palette = palette_teinte_melangee($couleur1, $couleur2, 7);
		$retour = var_export($palette, true);
		$html .= "<pre><strong>HUE BLEND - Couleurs {$couleur1}, {$couleur2}</strong> <br />{$retour}</pre>";
		foreach ($palette as $_couleur_bg) {
			$couleur_fg = '#' . couleur_inverser($_couleur_bg);
			$html .= "<li style=\"background-color:{$_couleur_bg}; color:{$couleur_fg}\">{$_couleur_bg}</li>";
		}
	}

	return $html;
}

function debug_detourer($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('action/editer_objet');
		include_spip('inc/carte_territoires');
		$id_carte = 5;
		$carte = objet_lire('carte_territoires', $id_carte);
		$ids = carte_territoires_detourer($id_carte);
		$retour = var_export($ids, true);
		$html .= "<pre><strong>{$id_carte}-{$carte['titre']}</strong> <br />{$retour}</pre>";

		$id_carte = 6;
		$carte = objet_lire('carte_territoires', $id_carte);
		$ids = carte_territoires_detourer($id_carte);
		$retour = var_export($ids, true);
		$html .= "<pre><strong>{$id_carte}-{$carte['titre']}</strong> <br />{$retour}</pre>";

		$id_carte = 9;
		$carte = objet_lire('carte_territoires', $id_carte);
		$ids = carte_territoires_detourer($id_carte);
		$retour = var_export($ids, true);
		$html .= "<pre><strong>{$id_carte}-{$carte['titre']}</strong> <br />{$retour}</pre>";

		$id_carte = 3;
		$carte = objet_lire('carte_territoires', $id_carte);
		$ids = carte_territoires_detourer($id_carte);
		$retour = var_export($ids, true);
		$html .= "<pre><strong>{$id_carte}-{$carte['titre']}</strong> <br />{$retour}</pre>";

		$id_carte = 4;
		$carte = objet_lire('carte_territoires', $id_carte);
		$ids = carte_territoires_detourer($id_carte);
		$retour = var_export($ids, true);
		$html .= "<pre><strong>{$id_carte}-{$carte['titre']}</strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_version_nomenclatures($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/ezcache_cache');
		$type_cache = 'reponse';
		$plugin = 'ezmashup';
		$type_requete = '';
		$collection = 'feeds';
		$filtres = [
			'sous_dossier' => $plugin
		];
		if ($type_requete) {
			$filtres['type_requete'] = $type_requete;
		}
		if ($collection) {
			$filtres['collection'] = $collection;
		}
		$caches = cache_repertorier('ezrest', $type_cache, $filtres);
		$retour = var_export($caches, true);
		$html .= "<pre><strong>{$plugin}/{$collection}</strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_contours($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/unite_peuplement');
		$plugin = 'contours';
		$type = 'zone';
		$pays = '';
		$feeds = unite_peuplement_informer_feeds($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'country';
		$pays = '';
		$feeds = unite_peuplement_informer_feeds($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'subdivision';
		$pays = '';
		$feeds = unite_peuplement_informer_feeds($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'subdivision';
		$pays = 'FR';
		$feeds = unite_peuplement_informer_feeds($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'protected_area';
		$pays = '';
		$feeds = unite_peuplement_informer_feeds($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'protected_area';
		$pays = 'FR';
		$feeds = unite_peuplement_informer_feeds($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_ezrest($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/ezcache_cache');
		$type_cache = 'reponse';
		$plugin = 'ezmashup';
		$type_requete = '';
		$collection = 'feeds';
		$filtres = [
			'sous_dossier' => $plugin
		];
		if ($type_requete) {
			$filtres['type_requete'] = $type_requete;
		}
		if ($collection) {
			$filtres['collection'] = $collection;
		}
		$caches = cache_repertorier('ezrest', $type_cache, $filtres);
		$retour = var_export($caches, true);
		$html .= "<pre><strong>{$plugin}/{$collection}</strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_territoires($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/territoires_feed');
		$plugin = 'territoires';
		$type = 'zone';
		$pays = '';
		$feeds = feeds_informer($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'country';
		$pays = '';
		$feeds = feeds_informer($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'subdivision';
		$pays = '';
		$feeds = feeds_informer($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'subdivision';
		$pays = 'FR';
		$feeds = feeds_informer($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'infrasubdivision';
		$pays = '';
		$feeds = feeds_informer($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'infrasubdivision';
		$pays = 'FR';
		$feeds = feeds_informer($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'protected_area';
		$pays = '';
		$feeds = feeds_informer($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";

		$type = 'protected_area';
		$pays = 'FR';
		$feeds = feeds_informer($plugin, $type, $pays);
		$retour = var_export($feeds, true);
		$html .= "<pre><strong>{$type}/{$pays}</strong> <br />{$retour}</pre>";
	}

	return $html;
}

function debug_idiome($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		$idiome = '';
		preg_match(_EXTRAIRE_IDIOME, $idiome, $matches);
		$retour = var_export($matches, true);
		$html .= "<pre>{$idiome} <br />{$retour}</pre>";

		$idiome = 'je suis juste un texte';
		preg_match(_EXTRAIRE_IDIOME, $idiome, $matches);
		$retour = var_export($matches, true);
		$html .= "<pre>{$idiome} <br />{$retour}</pre>";

		$idiome = '<:idiome_seul:>';
		preg_match(_EXTRAIRE_IDIOME, $idiome, $matches);
		$retour = var_export($matches, true);
		$html .= "<pre>{$idiome} <br />{$retour}</pre>";

		$idiome = '<:module:idiome:>';
		preg_match(_EXTRAIRE_IDIOME, $idiome, $matches);
		$retour = var_export($matches, true);
		$html .= "<pre>{$idiome} <br />{$retour}</pre>";
	}

	return $html;
}

function debug_mashup($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/ezmashup_dataset_source');
		$liste = feed_repertorier_categories('isocode');
		$retour = var_export($liste, true);
		$html .= "<pre>Liste des categories <br />{$retour}</pre>";
	}

	return $html;
}

function debug_showtable($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		$desc = [];
		if ($res = sql_query('SHOW COLUMNS FROM spip_feeds')) {
			$desc = sql_fetch_all($res);
		}
		$retour = var_export($desc, true);
		$html .= "<pre>DESCRIPTION TABLE SPIP_FEEDS = {$retour}</pre>";
	}

	return $html;
}
function debug_extraire_trads($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
//		include_spip("src/Texte/Collecteur/AbstractCollecteur");
		include_spip('src/Texte/Collecteur/Multis');
		$collecteurMultis = new Spip\Texte\Collecteur\Multis();

		$multis = [
			'',
			'<multi></multi>',
			'<multi> </multi>',
			'<multi>sans langue</multi>',
			'<multi>[fr]en français</multi>',
			'<multi>[fr]en français[en]en anglais</multi>',
			'<multi>en defaut[fr]en français[en]en anglais</multi>',
		];
		foreach ($multis as $multi) {
			$collecte = $collecteurMultis->collecter($multi);
			$trads = $collecte[0] ?? [];
			$retour = var_export($trads, true);
			$html .= "<pre>MULTI = <code>{$multi}<code> - TEXTE = {$retour}</pre>";
		}
	}

	return $html;
}

function debug_extraire_multi($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/filtres');
		$multi = '<multi>[en]Abbeville</multi>';
		$texte = extraire_multi($multi);

		$retour = var_export($texte, true);
		$html .= "<pre>MULTI = <code>{$multi}<code> - TEXTE = {$retour}</pre>";
	}

	return $html;
}

function debug_pluginspip_compter($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('pluginspip_fonctions');
		$entite = 'categorie';
		$id_depot = 0;
		$categorie = 'activite';
		$compatible_spip = '';
		$decompte = pluginspip_compter($entite, $id_depot, $categorie, $compatible_spip);

		$retour = var_export($decompte, true);
		$html .= "<pre>Decompte = {$retour}</pre>";
		$input = var_export(['entite' => $entite, 'depot' => $id_depot, 'categorie' => $categorie, 'spip' => $compatible_spip], true);
		$html .= "<pre>Input = {$input}</pre>";
	}

	return $html;
}

function debug_iucn_assessment($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('services/iucn/iucn_api');
		$search = [
			'tsn'             => 174394,
			'scientific_name' => 'Apteryx australis'
		];
		$donnees = iucn_get_assessment($search);

		$retour = var_export($donnees, true);
		$html .= "<pre>{$retour}</pre>";
	}

	return $html;
}

function vernacular_name($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/utils');
		$retour = '';
		$liste = [];
		if ($fichier = find_in_path('data/VernacularName.tsv')) {
			$langages = ['fr', 'en', 'es', 'pt', 'de', 'it'];
			$lignes = file($fichier);
			foreach ($lignes as $_cle => $_ligne) {
				$ligne = trim($_ligne);
				$champs = explode("\t", $ligne);
				if (in_array($champs[2], $langages) and $ligne) {
					$liste[$champs[2]][] = implode("\t", [$champs[0], $champs[1], $champs[4]]);
				}
			}

			foreach ($langages as $_langage) {
				$nb = 0;
				if (isset($liste[$_langage])) {
					file_put_contents(_DIR_TMP . 'vernaculars_' . $_langage . '.tsv', implode("\n", $liste[$_langage]));
					$nb = count($liste[$_langage]);
				}

				$retour .= '<p>' . $_langage . '=' . $nb . '</p>';
			}
		}

		$html .= "<pre>{$retour}</pre>";
	}

	return $html;
}

function debug_boussole($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/boussole');
		$liste = boussole_lire_consignation();
		$retour = var_export($liste, true);
		$html .= "<pre>{$retour}</pre>";

		$erreur = [];
		$acquerir = charger_fonction('boussole_phraser', 'inc');
		$donnees = $acquerir('bousarka', 'sarka', $erreur);
		$retour = var_export($donnees, true);
		$html .= "<pre>{$retour}</pre>";

		$erreur = [];
		$acquerir = charger_fonction('boussole_acquerir', 'inc');
		$donnees = $acquerir('spip', 'spip', $erreur);
		$retour = var_export($donnees, true);
		$html .= "<pre>{$retour}</pre>";
	}

	return $html;
}

function debug_xml($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/simplexml_to_array');

		$xml = '
<boussole alias="sarka" demo="https://www.sarka-spip.net/">

	<!-- Sites de reference Sarka-SPIP -->
	<groupe type="reference">
		<site alias="net" src="http://www.sarka-spip.net/" actif="oui" />
		<site alias="zest" src="http://zest.shizuka.fr/" actif="oui" />
	</groupe>

	<!-- Sites de decouverte -->
	<groupe type="decouverte">
		<site alias="delicious" src="http://www.delicious.com/sarkaspip/" actif="oui" />
	</groupe>

</boussole>
		';

		// Avec simplexml seul
		$objet_xml = simplexml_load_string($xml);
		$retour = var_export($objet_xml, true);
		$html .= "SIMPLEXML<br /><pre>{$retour}</pre>";

		// Avec json_encode et decode
		$objet_xml = simplexml_load_string("<dummyroot>{$xml}</dummyroot>");
		$tab1 = json_decode(json_encode($objet_xml), true);
		$retour = var_export($tab1, true);
		$html .= "<br />SIMPLEXML JSON<br /><pre>{$retour}</pre>";

		// Avec xml_to_array
		$parser = charger_fonction('simplexml_to_array', 'inc');
		$tab2 = $parser($xml);
		$retour = var_export($tab2, true);
		$html .= "<br />XML_TO_ARRAY<br /><pre>{$retour}</pre>";

		// Avec xml_parse
		include_spip('inc/xml');
		$tab3 = spip_xml_parse($xml, true);
		$retour = var_export($tab3, true);
		$html .= "<br />XML PARSE<br /><pre>{$retour}</pre>";

		$xml = [
			'a' => [
				'b' => 'toto',
				'c' => 'titi'
			],
			'd' => 2,
			'e' => 'tutu',
			'f' => [
				'g' => 'tata',
				'h' => [
					'i' => 0
				]
			]
		];

		$retour = var_export($xml, true);
		$html .= "<br />EZCACHE XML<br /><pre>{$retour}</pre>";

		// Avec la fonction de ezcache
		include_spip('ezcache/ezcache');
		$tab4 = ezcache_encoder_xml($xml);
		$retour = var_export($tab4, true);
//		$html .= "<br />EZCACHE ENCODER XML<br /><pre>{$retour}</pre>";

		// On décode avec ezcache
		$tab5 = ezcache_decoder_xml($tab4);
		$retour = var_export($tab5, true);
		$html .= "<br />EZCACHE DECODER XML<br /><pre>{$retour}</pre>";
	}

	return $html;
}

function debug_topnav($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('boussole_fonctions');

		$logo_top = boussole_logo_topnav('https://boussole.spip.net/plugins/boussole_spip/images/boussole/boussole-spip.png');
		$retour = var_export($logo_top, true);
		$html .= "<pre>{$retour}</pre>";
	}

	return $html;
}

function debug_svp($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/svp_cacher_local');

		$plugins = svp_descriptions_paquets_locaux($erreur);
		$retour = var_export($plugins, true);
		$html .= "<pre>{$retour}</pre>";
	}

	return $html;
}

function debug_gitea($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('services/gitea');

		$orga = 'spip-contrib-extensions';
		$repo = 'territoires';
//		$reponse = gitea_repo_readme($orga, $repo);
//		$retour = var_export($reponse, true);
//		$html .= "<b>$orga / $repo</b> : <br /><pre>$retour</pre>";
	}

	return $html;
}

function debug_discourse_api($actif) {
	$html = '';

	if ($actif == 'oui') {
		include_spip('services/discourse');

		$parametres_path = ['t', 152921, 'status'];
		$parametres_query = [];
		$options = ['status' => 'closed', 'enabled' => true];
		$url = discourse_construire_url($parametres_path, $parametres_query);
		$reponse = discourse_requeter($url, $options);
		$retour = var_export($reponse, true);
		$html .= "<b>{$url}</b> : <br /><pre>{$retour}</pre>";
		/*
				$parametres_url = array('groups');
				$url = discourse_construire_url($parametres_url);
				$input = var_export($parametres_url, true);
				$options = array();
				$reponse = discourse_requeter($url, $options);
				$retour = var_export($reponse, true);
				$html .= "<pre><b>$input</b> : $retour</pre>";

				$parametres_path = array('search');
				$parametres_query = array('q' => 'eric', 'type_filter' => 'users');
				$url = discourse_construire_url($parametres_path, $parametres_query);
				$input = $url;
				$options = array();
				$reponse = discourse_requeter($url, $options);
				$retour = var_export($reponse, true);
				$html .= "<pre><b>$input</b> : $retour</pre>";
		*/
	}

	return $html;
}

function debug_plugin_compter($actif) {
	$html = '';

	if ($actif == 'oui') {
		include_spip('pluginspip_fonctions');
		$id_depot = 0;
		$compatible_spip = '4.0';

		$categorie = 'activite';
		$decompte = plugin_compter($id_depot, $categorie, $compatible_spip);
		$retour = var_export($decompte, true);
		$html .= "<pre><b>{$compatible_spip} - {$categorie}</b> : {$retour}</pre>";

		$categorie = 'aucune';
		$decompte = plugin_compter($id_depot, $categorie, $compatible_spip);
		$retour = var_export($decompte, true);
		$html .= "<pre><b>{$compatible_spip} - {$categorie}</b> : {$retour}</pre>";

		$categorie = '';
		$decompte = plugin_compter($id_depot, $categorie, $compatible_spip);
		$retour = var_export($decompte, true);
		$html .= "<pre><b>{$compatible_spip} - {$categorie}</b> : {$retour}</pre>";

		plugin_informer_categorie('BIGUP', 'categorie');
	}

	return $html;
}

function debug_phraser_traductions($actif) {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/svp_phraser');
		$balise_traductions = '<traduction
			module="ncore"
			id="ncore--n-core-5311c"
			gestionnaire="salvatore"
			url="https://trad.spip.net"
			source="https://git.spip.net/spip-contrib-extensions/n-core.git"
			reference="fr">
			<langue code="en" url="https://trad.spip.net/tradlang_module/ncore-n-core?lang_cible=en" total="23" traduits="21" relire="0" modifs="0" nouveaux="2" pourcent="91.30">
				<traducteur nom="Eric Lupinacci" lien="https://trad.spip.net/auteur/_eric_" />
			</langue>
			<langue code="fr" url="https://trad.spip.net/tradlang_module/ncore-n-core?lang_cible=fr" total="23" traduits="23" relire="0" modifs="0" nouveaux="0" pourcent="100.00" />
		</traduction>
		<traduction
			module="paquet-ncore"
			id="paquet-ncore--n-core-5311c"
			gestionnaire="salvatore"
			url="https://trad.spip.net"
			source="https://git.spip.net/spip-contrib-extensions/n-core.git"
			reference="fr">
			<langue code="en" url="https://trad.spip.net/tradlang_module/ncore-paquet-xml-n-core?lang_cible=en" total="2" traduits="2" relire="0" modifs="0" nouveaux="0" pourcent="100.00">
				<traducteur nom="Eric Lupinacci" lien="https://trad.spip.net/auteur/_eric_" />
			</langue>
			<langue code="fr" url="https://trad.spip.net/tradlang_module/ncore-paquet-xml-n-core?lang_cible=fr" total="2" traduits="2" relire="0" modifs="0" nouveaux="0" pourcent="100.00" />
		</traduction>
		';
		$traductions = svp_phraser_traductions($balise_traductions);
		$retour = var_export($traductions, true);
		$html .= "<pre>{$retour}</pre>";
	}

	return $html;
}

function debug_compare_branches($actif) {
	$html = '';

	if ($actif == 'oui') {
		$importer = charger_fonction('importer_csv', 'inc');
		$fichier = find_in_path('data/compat.csv');
		$options = ['head' => true, 'delim' => "\t"];
		$infos = $importer($fichier, $options);

		include_spip('inc/svp_outiller');
		foreach ($infos as $_cle => $_info) {
			$branches = compiler_branches_spip($_info['compatibilite_spip']);
			$infos[$_cle]['branches_spip_new'] = $branches;
		}

		$exporter = charger_fonction('exporter_csv', 'inc');
		$options['entetes'] = array_keys($infos[0]);
		$options['envoyer'] = false;
		$exporter('compat_new', $infos, $options);

		$retour = var_export($infos, true);
		$html .= "<pre>{$retour}</pre>";
	}

	return $html;
}

function debug_version_compare($actif) {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/utils');

		$op = '<';
		$v1 = '3.0.1';
		$v2 = '3.0.*';
		$ok = spip_version_compare($v1, $v2, $op);
		$retour = var_export($ok, true);
		$html .= "<pre><b>{$v1} {$op} {$v2}</b> : {$retour}</pre>";

		$v2 = '3.0.2';
		$ok = spip_version_compare($v1, $v2, $op);
		$retour = var_export($ok, true);
		$html .= "<pre><b>{$v1} {$op} {$v2}</b> : {$retour}</pre>";

		$v2 = '3.0.0';
		$ok = spip_version_compare($v1, $v2, $op);
		$retour = var_export($ok, true);
		$html .= "<pre><b>{$v1} {$op} {$v2}</b> : {$retour}</pre>";
	}

	return $html;
}

function debug_compiler_branches($actif) {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/svp_outiller');

		$intervalle = '[4.0.0-alpha;4.0.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3;3.3.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.0;3.3.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.0.0;3.3]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.0.0;3]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.0.0;3.3.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[4.0.0-alpha;4.0.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.2.0;4.0.0[';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '];[';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[;[';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[1.8.0;4.0.*[';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '];]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[;]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>'{$intervalle}'</b> : {$retour}</pre>";

		$intervalle = '[3.2.0;4.0.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[4.0.0-alpha;4.0.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = ']3.2.0;4.0.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = ']3.2.999;4.0.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '];4.0.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[;4.0.*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = ']3.2.0;]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.2.0;]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.2.0;4.1.0[';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.1.0;3.2.0[';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[1.8.0;4.1.0]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[1.8.0;*]';
		$branches = compiler_branches_spip($intervalle);
		$retour = var_export($branches, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";
	}

	return $html;
}

function debug_extraire_bornes($actif) {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/svp_outiller');

		$intervalle = '';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>'{$intervalle}'</b> : {$retour}</pre>";

		$intervalle = '];]';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '];[';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.2.0;4.0.*]';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[4.0.0-alpha;4.0.*]';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = ']3.2.0;4.0.*]';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '];4.0.*]';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[;4.0.*]';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = ']3.2.0;]';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.2.0;]';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";

		$intervalle = '[3.2.0;4.1.0[';
		$bornes = extraire_bornes($intervalle, true);
		$retour = var_export($bornes, true);
		$html .= "<pre><b>{$intervalle}</b> : {$retour}</pre>";
	}

	return $html;
}

function debug_api_objet($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('base/objets');
		$table = 'spip_organisations_liens';
		$info = objet_type($table);
		$retour = var_export($info, true);
		$html .= "<pre><b>objet_type</b> : {$retour}</pre>";

		$info = table_objet($table);
		$retour = var_export($info, true);
		$html .= "<pre><b>table_objet</b> : {$retour}</pre>";

		$info = table_objet_sql($table);
		$retour = var_export($info, true);
		$html .= "<pre><b>table_objet_sql</b> : {$retour}</pre>";

		$info = id_table_objet($table);
		$retour = var_export($info, true);
		$html .= "<pre><b>id_table_objet</b> : {$retour}</pre>";

		$trouver_table = charger_fonction('trouver_table', 'base');
		$desc = $trouver_table($table);
		$retour = var_export($desc, true);
		$html .= "<pre><b>description {$table}</b> :<br />{$retour}</pre>";

		$table = 'spip_organisations';
		$trouver_table = charger_fonction('trouver_table', 'base');
		$desc = $trouver_table($table);
		$retour = var_export($desc, true);
		$html .= "<pre><b>description {$table}</b> :<br />{$retour}</pre>";

		$info = $GLOBALS['tables_jointures'];
		$retour = var_export($info, true);
		$html .= "<pre><b>tables de jointures</b> :<br />{$retour}</pre>";

		include_spip('inc/roles');
		$roles = roles_presents('organisation', 'auteur');
		$retour = var_export($roles, true);
		$html .= "<pre><b>Roles</b> :<br />{$retour}</pre>";
	}

	return $html;
}

function debug_parent($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('base/objets_parents');
		$parent = objet_trouver_parent('auteur', 1);
		$retour = var_export($parent, true);
		$html .= "<pre>auteur 1, parent : {$retour}</pre>";

		$parent = objet_trouver_parent('auteur', 10);
		$retour = var_export($parent, true);
		$html .= "<pre>auteur 10, parent : {$retour}</pre>";

		$parent = objet_trouver_parent('auteur', 2);
		$retour = var_export($parent, true);
		$html .= "<pre>auteur 2, parent : {$retour}</pre>";

		$parent = objet_trouver_parent('article', 1);
		$retour = var_export($parent, true);
		$html .= "<pre>article 1, parent : {$retour}</pre>";

		$parent = objet_trouver_parent('article', 2);
		$retour = var_export($parent, true);
		$html .= "<pre>article 2, parent : {$retour}</pre>";

		$objet = 'organisation';
		$type_enfant = type_objet_info_enfants($objet);
		$retour = var_export($type_enfant, true);
		$html .= "<pre>{$objet}, type des enfants : {$retour}</pre>";

		$enfants = objet_trouver_enfants('organisation', 5);
		$retour = var_export($enfants, true);
		$html .= "<pre>{$objet} organisation 5, enfants : {$retour}</pre>";
	}

	return $html;
}

function debug_config_orga($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/orgaunit_organisation');
		$config = organisation_configurer();
		$retour = var_export($config, true);
		$html .= "<pre>retour : {$retour}</pre>";
	}

	return $html;
}

function debug_config_log($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/framalog_log');
		log_configurer();
		$config = config_promise_lire('framalog');
		$retour = var_export($config, true);
		$html .= "<pre>retour : {$retour}</pre>";
	}

	return $html;
}

function debug_meta($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		$config = 1 << 24;
		$retour = var_export($config, true);
		$html .= "<pre>retour : {$retour}</pre>";
	}

	return $html;
}

function debug_config($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/config');

		$store = ['framtool' => 1];
		ecrire_config('/promise/debug', $store);

		$config = lire_config('/promise/debug');
		$retour = var_export($config, true);
		$html .= "<pre>retour : {$retour}</pre>";
	}

	return $html;
}

function debug_droit_autoriser($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/framusac_droit');
		$id_projet = 1;
		$retour = autoriser('modifier', 'projet', $id_projet);
		$retour = var_export($retour, true);
		$html .= "<pre>retour : {$retour}</pre>";

		$qui = 3;
		$retour = autoriser('creer', 'projet', 0, $qui, ['id_parent' => 1]);
		$retour = var_export($retour, true);
		$html .= "<pre>retour : {$retour}</pre>";

		$qui = 3;
		$retour = autoriser('creer', 'projet', 0, $qui);
		$retour = var_export($retour, true);
		$html .= "<pre>retour : {$retour}</pre>";
	}

	return $html;
}

function debug_droit_api($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		// Ajout d'un droit
		include_spip('action/editer_objet');
		$set = [
			'id_auteur'    => 2,
			'role'         => 'projectManager',
			'objet'        => 'projet',
			'id_objet'     => 0,
			'restrictions' => serialize([]),
		];
//		$id_droit = objet_inserer('droit', null, $set);
//		$html .= "<pre>retour : $id_droit</pre>";

		// Modification d'un droit
		$set = [
			'restrictions' => serialize(['activite' => '_noControl']),
		];
//		$retour = objet_modifier('droit', $id_droit, $set);
//		$html .= '<br />';
//		$html .= '<pre>retour : ' . ($retour == '' ? 'ok' : $retour) . '</pre>';

		$set = [
			'id_auteur'    => 4,
			'role'         => 'projectManager',
			'objet'        => 'projet',
			'id_objet'     => 2,
			'restrictions' => serialize([]),
		];
		$id_droit = objet_inserer('droit', null, $set);
		$html .= "<pre>retour : {$id_droit}</pre>";
	}

	return $html;
}

function debug_droit_lirecache($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/ezcache_cache');
		$caches = cache_repertorier('framusac', 'user_accesss', []);

		foreach ($caches as $_cache) {
			$cache = ['auteur' => $_cache['auteur']];
			$html .= "<h3>Auteur : {$_cache['auteur']}</h3>";
			$html .= bel_env(cache_lire('framusac', 'user_accesss', $cache), false);
		}
	}

	return $html;
}

function debug_droit_cacher($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		$auteurs = sql_allfetsel('id_auteur', 'spip_auteurs');

		include_spip('inc/framusac_droit');
		include_spip('inc/ezcache_cache');
		foreach ($auteurs as $_auteur) {
			droit_cacher($_auteur['id_auteur']);

			$cache = ['auteur' => $_auteur['id_auteur']];
			$html .= "<h3>Auteur : {$_auteur['id_auteur']}</h3>";
			$html .= bel_env(cache_lire('framusac', 'user_accesss', $cache), false);
		}
	}

	return $html;
}

function debug_droit_config($actif = 'non') {
	$html = '';

	if ($actif == 'oui') {
		include_spip('inc/framusac');
		include_spip('inc/config');

		autorisation_profil_charger();
		autorisation_perimetre_charger();
		autorisation_role_charger();

		$html = bel_env(lire_config('framusac', []), false);
	}

	return $html;
}

function debug_test_cache() {
	// Test des caches
	var_dump('CACHE');
	for ($i = 0; $i < 10; $i++) {
		$debut = debug_init_timer();
		$cache = [
			'sous_dossier' => 'noizetier',
			'objet'        => 'type_noisette',
			'fonction'     => 'contextes'
		];
		include_spip('inc/ezcache_cache');
		$descriptions = cache_lire('ncore', 'stockage', $cache);
		debug_affiche_timer($debut);
	}

	// Test d'une meta avec désérialisation
	var_dump('META');
	for ($i = 0; $i < 10; $i++) {
		$debut = debug_init_timer();
		$meta = sql_getfetsel('valeur', 'spip_meta', 'nom=' . sql_quote('plugin'));
		$valeur = unserialize($meta);
		debug_affiche_timer($debut);
	}

	return true;
}

function debug_init_timer() {
	return $timestamp_debut = microtime(true);
}
function debug_affiche_timer($timestamp_debut) {
	$timestamp_fin = microtime(true);
	$duree = $timestamp_fin - $timestamp_debut;
	var_dump($duree * 1000);
}

function _debug() {
	var_dump(2 ** 63, PHP_INT_MAX);
}
