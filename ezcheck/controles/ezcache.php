<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function config_ezcache($id_controle, $id_auteur, $options) {

	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On recharge la configuration des caches pour le plugin demandé
	if (!empty($options['plugin'])) {
		include_spip('ezcache/ezcache');
		ezcache_cache_configurer($options['plugin']);
	}

	return $erreur;
}
