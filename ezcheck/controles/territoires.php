<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function config_territoires($id_controle, $id_auteur, $options) {

	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On recharge la configuration statique du plugin
	if (!empty($options['recharger'])) {
		include_spip('territoires_administrations');
		// -- Configuration statique du plugin Territoires
		$config_statique = territoires_configurer('statique');
		// -- Mise à jour en meta
		territoires_adapter_config_statique($config_statique);
	}

	return $erreur;
}
