<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ce contrôle permet d'identifier les rendez-vous dont l'anonymisation est incomplète.
 * Cela peut résulter d'un job inachevé ou abandonné.
 *
 * @param int   $id_controle Identifiant du contrôle relatif à l'exécution de la fonction
 * @param int   $id_auteur   Identifiant de l'auteur ayant lancé le contrôle ou 0 si c'est un CRON
 * @param array $options     Paramètres nécessaires à l'exécution du contrôle
 *
 * @return string Erreur sous la forme d'une chaine (de type mot) ou vide si aucune erreur.
 */
function erdv_dashboard_erdvs_anonymisation(int $id_controle, int $id_auteur, array $options) : string {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// on acquière les champs anonymisés
	include_spip('inc/filtres');
	$spip_table_objet = table_objet_sql('erdv');
	$trouver_table = charger_fonction('trouver_table', 'base');
	$desc = $trouver_table($spip_table_objet);
	$anonymisation = array_keys($desc['champs_anonymises']);
	$champs_anonymises = implode(',', $anonymisation);
	
	// on liste les rendez-vous
	if ($result = sql_select('id_erdv,' . $champs_anonymises, "spip_erdvs", "statut = 'anonyme'", "", "date_fin DESC")){
		while ($row = sql_fetch($result)) {
			$id_erdv = $row['id_erdv'];
			// Initialisation par défaut d'une anomalie pour le type de contrôle
			$anomalie = [
				'type_controle' => 'erdv_dashboard_erdvs_anonymisation',
				'objet' => 'erdv',
				'id_objet' => $id_erdv
			];
			foreach ($row as $cle => $valeur) {
				if (isset($anonymisation[$cle])) {
					if ($anonymisation[$cle] == 'void' and $valeur != '') {
						// - on génère une anomalie
						$anomalie['code'] = $cle.'_void';
						// On en profite pour stocker dans les paramètres de l'anomalie
						// la valeur qu'elle devrait avoir
						$anomalie['parametres'] = '';
					}
				}
				// Si une anomalie a été détectée on l'ajoute dans la base
				if (!empty($anomalie['code'])) {
					// On crée l'anomalie
					observation_ajouter(true, $id_controle, $anomalie);
				}
			}
		}
	} else {
		// pas de rendez-vous anonymisé à vérifier, on log en debug
		spip_log('Pas de rendez-vous anonymisé à vérifier par le Dashboard', 'erdv.' . _LOG_DEBUG);
	}
	return $erreur;
}
