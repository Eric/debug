<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @throws Exception
 *
 * @return mixed|string
 */
function ezmashup_executer_feed($id_controle, $id_auteur, $options) {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On exécute le feed demandé
	if (!empty($options['feed_id'])) {
		include_spip('inc/ezmashup_feed');
		feed_executer('isocode', $options['feed_id']);
	}

	return $erreur;
}
