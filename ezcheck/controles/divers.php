<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function divers_jsonwheel($id_controle, $id_auteur, $options) {

	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On génère les JSON des wheels à partir des YAML
	if ($yamls = glob(_DIR_PLUGIN_DEBUG . 'wheels/*')) {
		include_spip('inc/yaml');
		include_spip('inc/flock');
		foreach ($yamls as $_yaml) {
			// On décode le YAML en tableau
			$wheel = yaml_decode(file_get_contents($_yaml), array('library' => 'sfyaml'));
			// On encode le tableau en JSON et on génère le fichier associé
			$json = json_encode($wheel);
			$jsonfile = str_replace('.yaml', '.json', $_yaml);
			ecrire_fichier($jsonfile, $json);
		}
	}


	return $erreur;
}
