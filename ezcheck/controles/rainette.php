<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function config_rainette_technique($id_controle, $id_auteur, $options) {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On recharge la configuration si demandé
	if ($options['recharger']) {
		include_spip('rainette_administrations');
		$configuration = configuration_technique_charger();

		// Ecriture de la configuration rechargée
		include_spip('inc/config');
		ecrire_config($options['meta'], $configuration);
	}

	return $erreur;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function config_rainette_service($id_controle, $id_auteur, $options) {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On recharge la configuration si demandé
	if ($options['recharger']) {
		include_spip('rainette_administrations');
		$service = $options['meta'];
		$configuration = configuration_service_charger($service);

		// Ecriture de la configuration rechargée
		include_spip('inc/config');
		$prefixe = $options['prefixe_meta'];
		ecrire_config("{$prefixe}{$service}", $configuration);
	}

	return $erreur;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return string
 */
function config_rainette_export($id_controle, $id_auteur, $options) {
	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On produit et affiche le tableau des comparaison de config
	if (!empty($options['bloc'])) {
		include_spip('rainette_fonctions');
		$services = array_keys(rainette_lister_services());

		include_spip('inc/rainette_normaliser');
		$export = [];
		foreach ($services as $_service) {
			$config = configuration_service_lire($_service, $options['bloc']);
			$config_donnees = $config['donnees'];
			foreach ($config_donnees as $_donnee => $_config) {
				$export[$_donnee][$_service] = $_config['cle'] ? 'natif' : '';
			}
		}

		ecrire_fichier(_DIR_TMP . 'rainette_export_' . $options['bloc'] . '.json', json_encode($export));
	}

	return $erreur;
}
