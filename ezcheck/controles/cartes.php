<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function config_cartes($id_controle, $id_auteur, $options) {

	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On recharge la configuration des caches pour le plugin demandé
	if (!empty($options['recharger'])) {
		include_spip('cartes_territoires_administrations');
		cartes_territoires_configurer();
	}

	return $erreur;
}
