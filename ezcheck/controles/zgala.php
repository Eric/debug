<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function zgala_afficher_defaut($id_controle, $id_auteur, $options) {

	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On recharge la configuration statique du plugin
	if (!empty($options['recharger'])) {
		include_spip('config/zgala');
		// -- Mise à jour en meta
		zgala_defaut_ecrire();
	}

	return $erreur;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function zgala_afficher_config($id_controle, $id_auteur, $options) {

	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// On recharge la configuration statique du plugin
	if (!empty($options['recharger'])) {
		include_spip('config/zgala');
		// -- Mise à jour en meta
		zgala_configuration_ecrire();
	}

	return $erreur;
}
