<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
return [
	// L
	'log_rainette_config_service' => 'Service "@service@" - Rechargement "@recharger@"',
	'log_rainette_config_technique' => 'Rechargement "@recharger@"',
];
