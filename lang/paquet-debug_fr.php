<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
return [
	// L
	'debug_nom' => 'Debusquer',
	'debug_slogan' => 'Module de debug pour le développement',
	'debug_description' => 'Ce plugin propose d\'une part, un dashboard de contrôles basé sur l\'API du plugin Check Factory et, d\'autre part, une page de débusquage dans l\'espace privé.',
];
